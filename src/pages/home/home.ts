import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavController, ToastController, Toast, LoadingController, Loading } from 'ionic-angular';
import { File } from '@ionic-native/file';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';

declare var cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public searchText: string = '';
  public searchResults: Array<{ any }> = [];
  public processing: boolean;
  public alert: Toast;
  public printMode: boolean = false;
  public loader: Loading;
  private events: Array<any> = [];

  constructor(public navCtrl: NavController, private http: Http,
    private toastCtrt: ToastController, private file: File,
    private loadingCtrt: LoadingController) {
      this.loader = this.loadingCtrt.create({
        content: 'Loading...'
      });
  }

  private unsubscribeEvents() {
    this.events.forEach(event => event.unsubscribe());
    this.events = [];
  }

  private get searchURL(): string {
    return `https://www.googleapis.com/customsearch/v1?key=AIzaSyC-IFkhfbkzFDg4LEKXGmxbGQ3bZcadXY8&cx=016914704424892180491:dpzyjxmxhfq&q=${this.searchText}`;
  }

  private getResultsHTML(): any {
    let HTML = '<div><div>';
    return new Promise((resolve, reject) => {
      Observable.forkJoin(this.searchResults.map((item: any) => this.http.get(item.link)))
        .subscribe(results => {
          results.forEach((result: any, i) => {
            HTML += `
          <div>${result._body}</div>
          <hr/>
      </div>`;
          })
          HTML += `</div>`;
          resolve(HTML)
        });
    })
  }

  public search() {
    this.searchResults = [];
    this.processing = true;
    this.unsubscribeEvents();
    let timeout = setTimeout(() => {
      if (timeout) clearTimeout(timeout);
      this.events[this.events.length] = this.http.get(this.searchURL)
        .map(res => res.json())
        .subscribe((res: any) => {
          this.processing = false;
          this.searchResults = (res.items || []).slice(0, 5);
        }, err => this.processing = false);
    }, 500);
  }

  public writeFile(file) {
    return this.file.writeFile(this.file.dataDirectory, `${this.searchText}-${new Date().getTime()}.pdf`, file);
  }

  public save() {
    this.loader.setContent('Generating PDF').present();
    this.getResultsHTML()
    .then(html => {
      if (cordova.plugins && cordova.plugins.pdf) {
        cordova.plugins.pdf.htmlToPDF({
          data: html,
          documentSize: "A4",
          landscape: "portrait",
          type: "base64"
        }, (base64File) => {
          this.writeFile(base64File)
          .then(res => {
            this.loader.dismiss();
            this.alert = this.toastCtrt.create({
              message: 'Successully generated',
              duration: 3000
            });
            this.alert.present();
          }, err => {
            this.loader.dismiss();
            this.alert = this.toastCtrt.create({
              message: JSON.stringify(err),
              duration: 3000
            });
            this.alert.present();
          })
        }, () => {
          this.alert = this.toastCtrt.create({
            message: 'Error generating pdf',
            duration: 3000
          });
          this.alert.present();
        });
      } else {
        this.printMode = true;
        this.processing = true;
        setTimeout(() => {
          print();
          this.printMode = false;
          this.processing = false;
        }, 500);
      }
    })
  }

}
